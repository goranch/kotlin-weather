package com.ex.goran.kotlinapp;

import java.text.SimpleDateFormat;

/**
 * Created by goran on 15/01/2017.
 */

public class Util {
    public static String convertEpochToDate(int unixTime) {

        long timeStamp = (long) unixTime * 1000;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
        return sdf.format(timeStamp);
    }
}
