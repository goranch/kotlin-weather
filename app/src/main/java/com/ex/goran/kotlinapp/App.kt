package com.ex.goran.kotlinapp

import android.app.Application
import com.ex.goran.kotlinapp.extensions.DelegatesExt

class App : Application() {

    companion object {
        var instance: App by DelegatesExt.notNullSingleValue()
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}