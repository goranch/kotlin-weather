package com.ex.goran.kotlinapp.domain.commands

import com.ex.goran.kotlinapp.api.ForecastRequest
import com.ex.goran.kotlinapp.domain.Command
import com.ex.goran.kotlinapp.domain.ForecastDataMapper
import com.ex.goran.kotlinapp.domain.model.ForecastList

class RequestForecastCommand(val zipCode: String): Command<ForecastList> {
    override fun execute(): ForecastList {
        val forecastRequest = ForecastRequest(zipCode)
        return ForecastDataMapper().convertFromDataModel(
                forecastRequest.execute()
        )
    }
}