package com.ex.goran.kotlinapp.api.model

/**
 * Created by goran on 12/01/2017.
 */
data class City(
        var id: Int? = null,
        var name: String? = null,
        var coord: Coord? = null,
        var country: String? = null,
        var population: Int? = null
)

class Coord(
        var lon: Double? = null,
        var lat: Double? = null
)

class list(

        var dt: Int,
        var temp: Temp? = null,
        var pressure: Double? = null,
        var humidity: Int? = null,
        var weather: List<Weather>? = null,
        var speed: Double? = null,
        var deg: Int? = null,
        var clouds: Int? = null,
        var rain: Double? = null,
        var snow: Double? = null

)

class Temp(

        var day: Double? = null,
        var min: Double? = null,
        var max: Double? = null,
        var night: Double? = null,
        var eve: Double? = null,
        var morn: Double? = null

)

class Weather(

        var id: Int? = null,
        var main: String? = null,
        var description: String? = null,
        var icon: String? = null

)

class WeatherResponse(

        var city: City? = null,
        var cod: String? = null,
        var message: Double? = null,
        var cnt: Int? = null,
        var list: List<list>? = null

)