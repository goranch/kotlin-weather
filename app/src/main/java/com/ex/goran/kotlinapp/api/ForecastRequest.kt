package com.ex.goran.kotlinapp.api

import android.util.Log
import com.ex.goran.kotlinapp.api.model.WeatherResponse
import com.google.gson.Gson
import java.net.URL

class ForecastRequest(val zipcode: String) {
    //http://api.openweathermap.org/data/2.5/forecast/daily?mode=json&units=metric&q=London,uk&appid=a7ffe509285a304791eda910f961f883
    companion object{
        private val APP_ID = "a7ffe509285a304791eda910f961f883"
        private val URL = "http://api.openweathermap.org/data/2.5/forecast/daily?mode=json&units=metric"
        private val COMPLETE_URL = "${URL}&appid=${APP_ID}&q="
    }

    fun execute(): WeatherResponse {
        val forecastJsonStr = URL(COMPLETE_URL + zipcode).readText()
        Log.d(javaClass.simpleName, forecastJsonStr)
        return Gson().fromJson(forecastJsonStr, WeatherResponse::class.java)
    }
}