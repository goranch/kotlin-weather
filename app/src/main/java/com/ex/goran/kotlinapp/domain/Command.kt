package com.ex.goran.kotlinapp.domain

interface Command<T> {
    fun execute(): T
}