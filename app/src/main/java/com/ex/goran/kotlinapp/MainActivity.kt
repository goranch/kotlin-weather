package com.ex.goran.kotlinapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.Toast
import com.ex.goran.kotlinapp.domain.commands.RequestForecastCommand
import kotlinx.android.synthetic.main.main_activity.*
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread
import java.net.URL


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        forecastList.layoutManager = LinearLayoutManager(this)


        async() {
            val result = RequestForecastCommand("W73QA").execute()
            uiThread {
                forecastList.adapter = ForecastListAdapter(result) {forecast ->  toast(forecast
                        .date)}
            }
        }


//        niceToast("iouio")
    }

    fun add(x: Int, y: Int): Int = x + y

    fun toast(message: String, length: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(this, message, length).show()
    }

    fun niceToast(message: String,
                  tag: String = MainActivity::class.java.simpleName,
                  length: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(this, "[$tag] $message", length).show()
    }

    class Request(val url: String) {
        fun run() {
            val forecastJsonString = URL(url).readText()
            Log.d(javaClass.simpleName, forecastJsonString)
        }
    }
}
