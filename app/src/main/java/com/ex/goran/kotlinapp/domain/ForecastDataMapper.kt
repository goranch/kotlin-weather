package com.ex.goran.kotlinapp.domain

import com.ex.goran.kotlinapp.Util
import com.ex.goran.kotlinapp.api.model.WeatherResponse
import com.ex.goran.kotlinapp.api.model.list
import com.ex.goran.kotlinapp.domain.model.Forecast
import com.ex.goran.kotlinapp.domain.model.ForecastList

/**
 * Created by goran on 13/01/2017.
 */
class ForecastDataMapper {
    fun convertFromDataModel(response: WeatherResponse): ForecastList {
        return ForecastList(response.city?.name, response.city?.country,
                convertForecastListToDomain(response.list!!))
    }

    private fun convertForecastListToDomain(list: List<list>):
            List<Forecast> {
        return list.map { convertForecastItemToDomain(it) }
    }

    private fun convertForecastItemToDomain(forecast: list): Forecast {
        return Forecast(
                convertDate(forecast.dt),
                forecast.weather?.get(0)?.description,
                forecast.temp?.max?.toInt()!!,
                forecast.temp!!.min!!.toInt(),
                generateIconUrl(forecast.weather?.get(0)?.icon)
        )
    }

    private fun generateIconUrl(icon: String?): String {
        return "http://openweathermap.org/img/w/$icon.png"
    }

    private fun convertDate(data: Int): String {
        return Util.convertEpochToDate(data)
//        val df = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault())
//        return df.format(data?.times(1000))
    }

}